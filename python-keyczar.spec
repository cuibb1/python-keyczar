%global _description\
Keyczar is an open source cryptographic toolkit designed to make it easier and\
safer for developers to use cryptography in their applications. Keyczar\
supports authentication and encryption with both symmetric and asymmetric keys.

Name:           python-keyczar
Version:        0.71c
Release:        13
Summary:        Toolkit for safe and simple cryptography
License:        ASL 2.0
URL:            http://www.keyczar.org/
Source0:        http://keyczar.googlecode.com/files/%{name}-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  python2-devel python2-crypto python2-pyasn1

%description %_description

%package -n python2-keyczar
Summary:        %summary
Requires:       python2-crypto python2-pyasn1
%{?python_provide:%python_provide python2-keyczar}

%description -n python2-keyczar %_description

%prep
%autosetup -n %{name}-%{version} -p1
rm -rf python_keyczar.egg-info

%build
%{__python} setup.py build

%check
cd tests/keyczar_tests
PYTHONPATH=$PYTHONPATH:../../src/ ./alltests.py

%install
%{__python} setup.py install -O1 --skip-build --root ${RPM_BUILD_ROOT}

%files -n python2-keyczar
%doc README doc/pycrypt.pdf
%license LICENSE
%{python_sitelib}/keyczar/
%{python_sitelib}/python_keyczar-*.egg-info
%{_bindir}/keyczart


%changelog
* Fri Nov 15 2019 sunguoshuai <sunguoshuai@huawei.com> - 0.71c-13
- Package init
